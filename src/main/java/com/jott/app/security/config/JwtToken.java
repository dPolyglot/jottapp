package com.jott.app.security.config;

import com.jott.app.security.TokenType;
import lombok.Data;

@Data
public class JwtToken {
    private String accessToken;
    private TokenType tokenType = TokenType.BEARER_TOKEN;

    public JwtToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
