package com.jott.app.data.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection="roles")
public class Role {
    @Id
    private String roleId;
    private RoleName roleName;

    public Role(RoleName roleName) {
        this.roleName = roleName;
    }
}
