package com.jott.app.data.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Data
@Document(collection="notes")
public class Note {
    @Id
    private String noteId;
    private String noteTitle;
    private String noteContent;
    private LocalDate dateCreated;
    private String noteImage;

    public Note(String noteTitle, String noteContent, LocalDate dateCreated){
        this.noteTitle= noteTitle;
        this.noteContent=noteContent;
        this.dateCreated=dateCreated;
    }

}
