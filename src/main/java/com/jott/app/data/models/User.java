package com.jott.app.data.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Data
@Document(collection="users")
public class User {
    @Id
    private String userId;

    private String username;
    private String firstName;
    private String lastName;
    private String verificationCode;

    private String email;
    private String password;
    private boolean isEnabled;
    private Set<Role> roles;
    private Set<Note> notes;


    public User(String firstName, String lastName, String username, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    /**
     * Adds new roles to user roles list
     *
     * @author Amaka
     * @param userRoles
     * */
    public void addRoles(Role... userRoles){
        if(this.roles == null) {
            this.roles = new HashSet<>();
        }
        this.roles.addAll(Arrays.asList(userRoles));
    }


}
