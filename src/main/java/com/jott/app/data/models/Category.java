package com.jott.app.data.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Data
@Document(collection = "categories")
public class Category {
    @Id
    private String categoryId;
    private String categoryName;
    private Set<Note> notes;

    public Category(String categoryName){
        this.categoryName = categoryName;
    }

    public Category(){}

    /**
     * Adds multiple notes to category object at once
     *
     * @author Amaka
     * @param userNotes
     * */
    public void addNotes(Note... userNotes){
        if(notes == null) {
            notes= new HashSet<>();
        }
        notes.addAll(Arrays.asList(userNotes));
    }
}
