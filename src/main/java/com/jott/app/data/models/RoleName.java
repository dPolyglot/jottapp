package com.jott.app.data.models;

public enum RoleName {
    ROLE_USER, ROLE_ADMIN;

    @Override
    public String toString(){
        switch (this) {
            case ROLE_USER:
                return "role_user";
            case ROLE_ADMIN:
                return "role_admin";
            default:
                throw new IllegalArgumentException();
        }
    }
}
