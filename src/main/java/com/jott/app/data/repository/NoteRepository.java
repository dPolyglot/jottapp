package com.jott.app.data.repository;

import com.jott.app.data.models.Note;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NoteRepository extends MongoRepository<Note, String> {
    Note findNoteByNoteTitle(String title);

}
