package com.jott.app.data.repository;

import com.jott.app.data.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserRepository extends MongoRepository<User, String> {
    Optional<User> findUserByEmailOrUsername(String email, String username);

    User findUserByUsername(String username);

    User findUserByEmail(String email);

    Boolean existsByEmail(String email);

    Boolean existsByUsername(String username);

    Optional<User> findUserByVerificationCode(String verificationCode);

}
