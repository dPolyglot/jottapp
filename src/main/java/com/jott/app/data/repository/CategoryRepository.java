package com.jott.app.data.repository;

import com.jott.app.data.models.Category;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends MongoRepository<Category, String> {
    Category findCategoryByCategoryName(String categoryName);

    Boolean existsByCategoryName(String categoryName);
}
