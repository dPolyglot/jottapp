package com.jott.app.data.repository;

import com.jott.app.data.models.Role;
import com.jott.app.data.models.RoleName;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RoleRepository extends MongoRepository<Role, String> {
    Optional<Role> findByRoleName(RoleName role);
}
