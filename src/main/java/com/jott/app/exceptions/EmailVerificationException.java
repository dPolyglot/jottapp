package com.jott.app.exceptions;

public class EmailVerificationException extends JottException{
    public EmailVerificationException(String message){ super(); }
}
