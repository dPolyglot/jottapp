package com.jott.app.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class JottException extends Exception{
    public JottException() {
    }

    public JottException(String message) {
        super(message);
    }

    public JottException(String message, Throwable cause) {
        super(message, cause);
    }

    public JottException(Throwable cause) {
        super(cause);
    }
}
