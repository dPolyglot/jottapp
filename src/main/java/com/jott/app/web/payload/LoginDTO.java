package com.jott.app.web.payload;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
public class LoginDTO {

    @NotBlank
    @Email(message = "Invalid email or password")
    private String usernameOrEmail;
    @NotBlank
    private String password;
}
