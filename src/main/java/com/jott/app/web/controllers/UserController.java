package com.jott.app.web.controllers;

import com.jott.app.data.models.Role;
import com.jott.app.data.models.User;
import com.jott.app.data.repository.RoleRepository;
import com.jott.app.data.repository.UserRepository;
import com.jott.app.exceptions.JottException;
import com.jott.app.security.JwtTokenProvider;
import com.jott.app.security.config.JwtToken;
import com.jott.app.web.payload.ApiResponse;
import com.jott.app.web.payload.LoginDTO;
import com.jott.app.web.payload.SignupDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Collections;

import static com.jott.app.data.models.RoleName.ROLE_USER;

@RestController
@RequestMapping("/api/users")
@Slf4j
public class UserController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userDb;

    @Autowired
    RoleRepository roleDb;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;


    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginDTO loginDTO) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginDTO.getUsernameOrEmail(),
                        loginDTO.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtToken(jwt));

    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupDTO signUpDTO) throws JottException {
        if(userDb.existsByUsername(signUpDTO.getUsername())) {
            return new ResponseEntity<>(new ApiResponse(false, "Username is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }

        if(userDb.existsByEmail(signUpDTO.getEmail())) {
            return new ResponseEntity<>(new ApiResponse(false, "Email Address already in use!"),
                    HttpStatus.BAD_REQUEST);
        }

        // Creating user's account
        log.info("signUp req ---> {}", signUpDTO);
        User user = new User(
                signUpDTO.getFirstName(),
                signUpDTO.getLastName(),
                signUpDTO.getUsername(),
                signUpDTO.getEmail(),
                passwordEncoder.encode(signUpDTO.getPassword())
        );

        Role userRole = roleDb.findByRoleName(ROLE_USER)
                .orElseThrow(() -> new JottException("User Role not set"));

        user.setRoles(Collections.singleton(userRole));

        log.info("Created user -> {}", user);

        User result = userDb.save(user);

        log.info("Created user result -> {}", result);

        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/users/{username}")
                .buildAndExpand(result.getUsername()).toUri();

        return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
    }

}
