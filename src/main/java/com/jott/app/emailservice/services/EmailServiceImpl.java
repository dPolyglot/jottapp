package com.jott.app.emailservice.services;

import com.jott.app.data.models.User;
import com.jott.app.data.repository.UserRepository;
import com.jott.app.emailservice.utils.MailCreator;
import com.jott.app.emailservice.utils.MailCreatorHelper;
import com.jott.app.exceptions.EmailVerificationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.Optional;

@Slf4j
public class EmailServiceImpl implements EmailService{

    @Autowired
    UserRepository userRepository;

    @Autowired
    JavaMailSender javaMailSender;

    @Autowired
    MailCreator mailCreator;


    @Override
    public void sendActivateAccountMessage(User user, String URL) throws MessagingException {
        javaMailSender.send(mailCreator.createActivateAccountMessage(user, URL));
        log.info("Mail sent");
    }

    @Override
    public boolean verify(String verificationCode, HttpServletRequest request) throws EmailVerificationException, UnsupportedEncodingException, MessagingException {
        Optional<User> user = userRepository.findUserByVerificationCode(verificationCode);

        log.info("user with verification code --> {}", user.get());

        if(user.isEmpty()){
            throw new EmailVerificationException("This link is broken or invalid");
        }
        else{
            user.get().setEnabled(true);
            user.get().setVerificationCode(null);
            sendConfirmationEmail(user.get(), MailCreatorHelper.getSiteURL(request));
            userRepository.save(user.get());
            return true;
        }
    }

    @Override
    public void sendResetPasswordMessage(User user, String URL) throws MessagingException {
        javaMailSender.send(mailCreator.createResetPasswordMessage(user, URL));
    }

    private void sendConfirmationEmail(User user, String siteURL)
            throws MessagingException, UnsupportedEncodingException {
        javaMailSender.send(mailCreator.createAccountActivationConfirmationEmail(user, siteURL));
        log.info("email was sent");
    }
}
