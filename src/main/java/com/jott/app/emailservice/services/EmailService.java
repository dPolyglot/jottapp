package com.jott.app.emailservice.services;

import com.jott.app.data.models.User;
import com.jott.app.exceptions.EmailVerificationException;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;

@Service
public interface EmailService {
    void sendActivateAccountMessage(User user, String URL) throws MessagingException;

    boolean verify(String verificationCode, HttpServletRequest request) throws EmailVerificationException, UnsupportedEncodingException, MessagingException;

    void sendResetPasswordMessage(User user, String URL) throws MessagingException;

}
