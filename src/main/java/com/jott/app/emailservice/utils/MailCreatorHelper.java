package com.jott.app.emailservice.utils;

import lombok.extern.slf4j.Slf4j;
import org.modelmapper.internal.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.servlet.http.HttpServletRequest;
import java.util.Properties;

@Slf4j
@Service
public class MailCreatorHelper {
    @Autowired
    JavaMailSender javaMailSender;

    public static String getSiteURL(HttpServletRequest request){
        String siteURL = request.getRequestURL().toString();
        return siteURL.replace(request.getServletPath(), "");
    }

    private String setVerificationCodeForUser(){
        log.info("Generating authentication code --------------------------------");
        return RandomString.make(35);
    }

    private void setProperties(){
        Properties props = new Properties();
        props.setProperty("mail.host", "localhost");
        props.setProperty("mail.smtp.port", "587");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable", "true");
        Authenticator auth = new SMTPAuthenticator("saucekoder@gmail.com", "jott");

        Session session = Session.getInstance(props, auth);
    }




    private class SMTPAuthenticator extends Authenticator
    {
        private PasswordAuthentication authentication;

        public SMTPAuthenticator(String login, String password)
        {
            authentication = new PasswordAuthentication(login, password);
        }

        @Override
        protected PasswordAuthentication getPasswordAuthentication()
        {
            return authentication;
        }
    }
}
