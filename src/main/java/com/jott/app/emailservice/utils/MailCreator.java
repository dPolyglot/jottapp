package com.jott.app.emailservice.utils;

import com.jott.app.data.models.User;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;


public interface MailCreator {
    MimeMessage createActivateAccountMessage(User user, String URL) throws MessagingException;
    MimeMessage createResetPasswordMessage(User user, String URl) throws MessagingException;

    MimeMessage createAccountActivationConfirmationEmail(User user, String siteURL) throws UnsupportedEncodingException, MessagingException;
}
