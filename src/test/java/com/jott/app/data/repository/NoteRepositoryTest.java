package com.jott.app.data.repository;

import com.jott.app.data.models.Note;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
@Slf4j
class NoteRepositoryTest {
    @Autowired
    NoteRepository noteRepository;

    @Test
    @DisplayName("Note is saved to database")
    void noteCanBeSaved(){

        Note note1 = new Note("Food in boston", "i love boston dishes", LocalDate.now());
        noteRepository.save(note1);
        assertThat(noteRepository.count()).isGreaterThan(0);
        assertThat(noteRepository.findNoteByNoteTitle("Food in boston")).isNotNull();

        log.info("First jott note -> {}", note1);
    }

    @Test
    @DisplayName("Existing note can be read from database")
    void noteCanBeViewed(){
        Note existingNote = noteRepository.findNoteByNoteTitle("Food in boston");

        assertThat(existingNote.getNoteContent()).isEqualTo("i love boston dishes");
    }

    @Test
    @DisplayName("Existing note can be updated and saved to the database")
    void noteCanBeUpdated(){
        Note existingNote = noteRepository.findNoteByNoteTitle("Food in boston");
        existingNote.setNoteContent("I also want to try scandinavian dishes.");
        noteRepository.save(existingNote);

        assertThat(existingNote.getNoteContent()).isEqualTo("I also want to try scandinavian dishes.");

        log.info("Updated note -> {}", existingNote.getNoteContent());
    }

    @Test
    @DisplayName("Existing note can be deleted from database")
    void noteCanBeDeleted(){

        Note existingNote = noteRepository.findNoteByNoteTitle("Food in boston");

        log.info("Before deleting note -> {}", existingNote);

        noteRepository.deleteById("61686b464195b903449f9e1e");

        log.info("After deleting note -> {}", existingNote);

        assertThat(noteRepository.findNoteByNoteTitle("Food in boston")).isNull();
    }
}