package com.jott.app.data.repository;

import com.jott.app.data.models.Category;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
@Slf4j
class CategoryRepositoryTest {

    Category food;

    @Autowired
    CategoryRepository categoryRepository;


    @BeforeEach
    void setUp(){
        food = new Category("food in boston");
        categoryRepository.save(food);
    }

    @AfterEach
    void tearDown(){
        categoryRepository.deleteAll();
    }


    @Test
    @DisplayName("Category is saved to database")
    void categoryCanBeSaved(){

        Category food2 = new Category("food in london");

        categoryRepository.save(food2);

        log.info("First jott category -> {}", food2);
        assertThat(categoryRepository.findCategoryByCategoryName("food in london")).isNotNull();
    }

    @Test
    @DisplayName("Existing category info can be read from database")
    void categoryCanBeViewed(){
        Category food1 = categoryRepository.findCategoryByCategoryName("food in boston");

        assertThat(food1.getCategoryName()).isEqualTo("food in boston");
    }

    @Test
    @DisplayName("Existing category can be updated and saved to the database")
    void categoryCanBeUpdated(){
        Category food1 = categoryRepository.findCategoryByCategoryName("food in boston");
        food1.setCategoryName("food in my belly");
        categoryRepository.save(food1);

        assertThat(food1.getCategoryName()).isNotEqualTo("food");
        assertThat(food1.getCategoryName()).isEqualTo("food in my belly");

        log.info("Updated category -> {}", food1);
    }

    @Test
    @DisplayName("Existing category can be deleted from database")
    void categoryCanBeDeleted(){

        Category food1 = categoryRepository.findCategoryByCategoryName("food in boston");

        assertThat(food1).isNotNull();
        assertThat(food1.getCategoryId()).isNotNull();

        log.info("Before deleting category -> {}  with id --> {}", food1, food1.getCategoryId());

        categoryRepository.deleteById(food1.getCategoryId());

        log.info("After deleting category -> {}", food1);

        assertThat(categoryRepository.existsByCategoryName(food1.getCategoryName())).isFalse();
    }
}