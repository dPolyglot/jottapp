package com.jott.app.data.repository;

import com.jott.app.data.models.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
@Slf4j
class UserRepositoryTest {

    User user;

    @Autowired
    UserRepository userRepository;

    @BeforeEach
    void setUp(){
        user = new User("Adam", "Scott", "dscotty1", "scotty@gmail.com", "scoop223");
        userRepository.save(user);
    }

    @AfterEach
    void tearDown(){
        userRepository.deleteAll();
    }

    @Test
    @DisplayName("User is saved to database")
    void userCanBeSaved(){

        User adam = new User("Adamy", "Scottsdale", "dscottys1", "scottsydale@gmail.com", "scoopy223");
        userRepository.save(adam);
        assertThat(userRepository.findUserByUsername("dscottys1")).isNotNull();

        log.info("First jott user -> {}", adam);
    }

    @Test
    @DisplayName("Existing user info can be read from database")
    void userCanBeViewed(){
        User adam = userRepository.findUserByUsername("dscotty1");

        assertThat(adam.getFirstName()).isEqualTo("Adam");
        assertThat(adam.getEmail()).isEqualTo("scotty@gmail.com");
    }

    @Test
    @DisplayName("Existing user can be updated and saved to the database")
    void userCanBeUpdated(){
        User adam = userRepository.findUserByUsername("dscotty1");
        adam.setFirstName("Liam");
        userRepository.save(adam);

        assertThat(adam.getFirstName()).isNotEqualTo("Adam");
        assertThat(adam.getFirstName()).isEqualTo("Liam");

        log.info("Updated user -> {}", adam);
    }

    @Test
    @DisplayName("Existing user can be deleted from database")
    void userCanBeDeleted(){

        User liam = userRepository.findUserByUsername("dscotty1");

        log.info("Before deleting user -> {}", liam);

        userRepository.deleteById(liam.getUserId());

        log.info("After deleting user -> {}", liam);

        assertThat(userRepository.existsByUsername("dscottys1")).isFalse();
    }
}